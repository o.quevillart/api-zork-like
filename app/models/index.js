const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.scene = require("../models/scene.model.js")(sequelize, Sequelize);
db.situation = require("../models/situation.model.js")(sequelize, Sequelize);
db.personnage = require("../models/personnage.model.js")(sequelize, Sequelize);
db.objet = require("../models/objet.model.js")(sequelize, Sequelize);

// relation scene situation

db.situation.belongsToMany(db.objet, {
  through: "situation_objet",
  foreignKey: "situationId",
  otherKey: "objetId"
});

db.objet.belongsToMany(db.situation, {
  through: "situation_objet",
  foreignKey: "objetId",
  otherKey: "situationId"
});

// relation scene situation

db.scene.belongsToMany(db.situation, {
  through: "scene_situation",
  foreignKey: "sceneId",
  otherKey: "situationId"
});
db.situation.belongsToMany(db.scene, {
  through: "scene_situation",
  foreignKey: "situationId",
  otherKey: "sceneId"
});

// relation perso et situation

db.situation.belongsToMany(db.personnage, {
  through: "situation_enemis",
  foreignKey: "situationId",
  otherKey: "personnageId"
});
db.personnage.belongsToMany(db.situation, {
  through: "situation_enemis",
  foreignKey: "personnageId",
  otherKey: "situationId"
});

// relation role user

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;
