module.exports = (sequelize, Sequelize) => {
    const Personnage = sequelize.define("personnage", {
      
      name: {
        type: Sequelize.STRING,
        unique: true
      },
      life: {
        type: Sequelize.INTEGER
      },
      attack: {
        type: Sequelize.INTEGER
      },
      equiped: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Personnage;
  };