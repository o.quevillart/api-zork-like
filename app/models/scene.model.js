module.exports = (sequelize, Sequelize) => {
    const Scene = sequelize.define("scene", {
      name: {
        type: Sequelize.STRING,
        unique: true
      }
    });
  
    return Scene;
  };