module.exports = (sequelize, Sequelize) => {
    const Objet = sequelize.define("objet", {
      name: {
        type: Sequelize.STRING,
        unique: true
      },
      equipable: {
        type: Sequelize.BOOLEAN
      },
      object_type: {
        type: Sequelize.STRING
      },
      damage: {
        type: Sequelize.INTEGER
      },
      durability: {
        type: Sequelize.INTEGER
      },
      unique_usage: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Objet;
  };