module.exports = (sequelize, Sequelize) => {
    const Situation = sequelize.define("situation", {
      name: {
        type: Sequelize.STRING,
        unique: true
      },
      text: {
        type: Sequelize.STRING
      },
    });
  
    return Situation;
  };