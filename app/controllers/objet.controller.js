const db = require("../models");

const Objet = db.objet;
const Situation = db.situation;
const Op = db.Sequelize.Op;


exports.objetCreate = (req, res) => {
  
  Objet.create({
    name: req.body.name,
    equipable: req.body.equipable,
    object_type: req.body.object_type,
    damage: req.body.damage,
    durability: req.body.durability,
    unique_usage: req.body.unique_usage
  })
  .then(obj => {
    if (req.body.situation) {
      console.log(Situation);
      Situation.findAll({
        where: {
          name: {
            [Op.or]: req.body.situation
          }
        },
        raw: true
      }).then(situation => {
        for (let i = 0; i < situation.length; i++) {
          const element = situation[i];
          console.log(element);
          obj.setSituations(element.id);
        }
      }).then(data => {
          res.send({ message: "Object was registered successfully!" , data: data});
      });
    } else {
      // obj role = 1
      obj.setSituations([1]).then(data => {
        res.send({ message: "Object was registered successfully!" , data: data});
      });
    }
  })
  .catch(err => {
    res.status(500).send({ message: err.message });
  });
  };

  exports.findAll = (req, res) => {
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
  
    Objet.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Objets."
        });
      });
  };
  
  exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Objet.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Objet with id=" + id
        });
      });
  };
  
  exports.update = (req, res) => {
    const id = req.params.id;
  
    Objet.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Objet was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Objet with id=${id}. Maybe Objet was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Objet with id=" + id
        });
      });
  };
  
  exports.delete = (req, res) => {
    const id = req.params.id;
  
    Objet.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Objet was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Objet with id=${id}. Maybe Objet was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Objet with id=" + id
        });
      });
  };
  
  exports.deleteAll = (req, res) => {
    Objet.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Objets were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Objets."
        });
      });
  };