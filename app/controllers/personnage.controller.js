const db = require("../models");

const Personnage = db.personnage;
const Situation = db.situation;

const Op = db.Sequelize.Op;

exports.personnageCreate = (req, res) => {
  
  Personnage.create({
    name: req.body.name,
    life: req.body.life,
    attack: req.body.attack,
    equiped: req.body.equiped
  })
.then(perso => {
  if (req.body.situation) {
    console.log(Situation);
    Situation.findAll({
      where: {
        name: {
          [Op.or]: req.body.situation
        }
      },
      raw: true
    }).then(situation => {
      for (let i = 0; i < situation.length; i++) {
        const element = situation[i];
        console.log(element);
        perso.setSituations(element.id)
      }
    })
      .then(data => {
        res.send({ message: "Personnage was registered successfully!", data: data });
      });
  } else {
    // perso role = 1
    perso.setSituations([1]).then(data => {
      res.send({ message: "Personnage was registered successfully!" , data: data});
    });
  }
})
.catch(err => {
  res.status(500).send({ message: err.message });
});
};

exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  Personnage.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Personnages."
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Personnage.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Personnage with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Personnage.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Personnage was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Personnage with id=${id}. Maybe Personnage was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Personnage with id=" + id
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Personnage.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Personnage was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Personnage with id=${id}. Maybe Personnage was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Personnage with id=" + id
      });
    });
};

exports.deleteAll = (req, res) => {
  Personnage.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Personnages were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Personnages."
      });
    });
};