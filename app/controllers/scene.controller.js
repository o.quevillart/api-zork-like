
const db = require("../models");

const Scene = db.scene;


exports.sceneCreate = (req, res) => {
  
  Scene.create({
    name: req.body.name
  })
  .then(data => {
    console.log(data)
    res.send(
      { message: "Scene was registered successfully!", data: data }
    );
  })
  .catch(err => {
    res.status(500).send({ message: err.message });
  });
    
};
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  Scene.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Scenes."
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Scene.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Scene with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Scene.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Scene was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Scene with id=${id}. Maybe Scene was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Scene with id=" + id
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Scene.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Scene was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Scene with id=${id}. Maybe Scene was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Scene with id=" + id
      });
    });
};

exports.deleteAll = (req, res) => {
  Scene.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Scenes were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Scenes."
      });
    });
};