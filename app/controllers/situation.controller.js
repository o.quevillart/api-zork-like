const db = require("../models");

const Situation = db.situation;
const Scene = db.scene;
const Op = db.Sequelize.Op;

exports.situationCreate = (req, res) => {
  
  Situation.create({
    name: req.body.name,
    text: req.body.text
  })
  .then(situ => {
    if (req.body.scene) {
      Scene.findAll({
        where: {
          name: {
            [Op.or]: req.body.scene
          }
        },
        raw: true
      })
      .then(scene => {
        console.log("scene : " + scene)
        for (let i = 0; i < scene.length; i++) {
          const element = scene[i];
          console.log(element);
          situ.setScenes(element.id);
        }
      }).then(data => {
        console.log("data : " + data)
        res.send({ message: "Situation was registered successfully scene" , data: situ});
      });
    } else {
      
      // situ role = 1
      situ.setScenes([1]).then(data => {
        res.send({ message: "Situation was registered successfully! pas scene" , data: situ});
      });
    }
  })
  .catch(err => {
    res.status(500).send({ message: err.message });
  });
  };
  
  exports.findAll = (req, res) => {
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
  
    Situation.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Scenes."
        });
      });
  };
  
  exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Situation.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Situation with id=" + id
        });
      });
  };
  
  exports.update = (req, res) => {
    const id = req.params.id;
  
    Situation.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Situation was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Situation with id=${id}. Maybe Situation was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Situation with id=" + id
        });
      });
  };
  
  exports.delete = (req, res) => {
    const id = req.params.id;
  
    Situation.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Situation was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Situation with id=${id}. Maybe Situation was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Situation with id=" + id
        });
      });
  };
  
  exports.deleteAll = (req, res) => {
    Situation.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Scenes were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Scenes."
        });
      });
  };