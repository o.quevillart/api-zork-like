const controller = require("../controllers/situation.controller");
const { authJwt } = require("../middleware");
module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/situation/",[authJwt.verifyToken],controller.situationCreate);
  app.get("/api/situation/",[authJwt.verifyToken],controller.findAll);
  app.get("/api/situation/:id",[authJwt.verifyToken],controller.findOne);
  app.put("/api/situation/:id",[authJwt.verifyToken],controller.update);
  app.delete("/api/situation/:id",[authJwt.verifyToken],controller.delete);
  app.delete("/api/situation/",[authJwt.verifyToken],controller.deleteAll);
};