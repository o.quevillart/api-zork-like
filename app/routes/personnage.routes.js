const controller = require("../controllers/personnage.controller");
const { authJwt } = require("../middleware");
module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/personnage/",[authJwt.verifyToken],controller.personnageCreate);
  app.get("/api/personnage/",[authJwt.verifyToken],controller.findAll);
  app.get("/api/personnage/:id",[authJwt.verifyToken],controller.findOne);
  app.put("/api/personnage/:id",[authJwt.verifyToken],controller.update);
  app.delete("/api/personnage/:id",[authJwt.verifyToken],controller.delete);
  app.delete("/api/personnage/",[authJwt.verifyToken],controller.deleteAll);
};
