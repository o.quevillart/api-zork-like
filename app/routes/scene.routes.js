const controller = require("../controllers/scene.controller");
const { authJwt } = require("../middleware");
module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/scene/",[authJwt.verifyToken],controller.sceneCreate);
  app.get("/api/scene/",[authJwt.verifyToken],controller.findAll);
  app.get("/api/scene/:id",[authJwt.verifyToken],controller.findOne);
  app.put("/api/scene/:id",[authJwt.verifyToken],controller.update);
  app.delete("/api/scene/:id",[authJwt.verifyToken],controller.delete);
  app.delete("/api/scene/",[authJwt.verifyToken],controller.deleteAll);
};
