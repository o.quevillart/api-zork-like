const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "*"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// database
const db = require("./app/models");
// faire plus propre !!
setTimeout(() => {
  db.sequelize.sync();
}, 10000);

  db.sequelize.sync();


// simple route
app.get("/", (req, res) => {
  res.json({ message: "ZOOoooRrk" });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/scene.routes')(app);
require('./app/routes/situation.routes')(app);
require('./app/routes/personnage.routes')(app);
require('./app/routes/objet.routes')(app);

// set port, listen for requests
// const PORT = process.env.PORT || 8080;
// app.listen(PORT, () => {
//   console.log(`Server is running on port ${PORT}.`);
// });
module.exports = app