const http = require("http");

const apiCallHome = async () => {
    const call = await http.get('http://localhost:18080/');
    return call;
}

const getCallHome = async () => {
    const test = await getFunctionsHome.apiCallHome();
    return test;
}
const getFunctionsHome = {apiCallHome,getCallHome}

describe('test api home', () => {
    test('first call', async () =>{
        getFunctionsHome.apiCallHome = jest.fn().mockReturnValue({message: "ZOOoooRrk"})
        const test = await getFunctionsHome.getCallHome();
        expect(test.message).toBe("ZOOoooRrk");
    })
})