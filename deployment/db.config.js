module.exports = {
    HOST: "db",
    USER: "admin",
    PASSWORD: "admin",
    DB: "zork",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };