FROM node:12.18

COPY . .

RUN npm install

CMD node index.js

RUN npm test

EXPOSE 8080