# Zork-CDA #

## ToDo : ##

### BDD ###

- créer la bdd
- sequelize s'occupe de tout le reste

### API ###

- JWT
- routes
- tables et relations
- contenu

### APP ###

- interface
- connexion api
- algo

## FEATURE : ##

- une aventure générée aléatoirement car on aime ça.
- un système de seed.
- système d'inventaire.
- interface textuelle dans un premier temps.
- interface visuelle ensuite
- système de save ??